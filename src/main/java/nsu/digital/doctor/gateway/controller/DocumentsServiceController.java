package nsu.digital.doctor.gateway.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nsu.digital.doctor.gateway.config.DocumentsServiceRedirectSettings;
import nsu.digital.doctor.gateway.security.JwtService;
import nsu.digital.doctor.gateway.service.UserService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Base64;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/documents-service")
@Slf4j
@RequiredArgsConstructor
public class DocumentsServiceController {

    private final RestTemplate restTemplate;

    private final DocumentsServiceRedirectSettings documentsServiceRedirectSettings;
    private final UserService userService;
    private final JwtService jwtService;
    private final ObjectMapper objectMapper;

    private static final byte[] PRIVATE_KEY = Base64.getEncoder().encode(UUID.randomUUID().toString().getBytes());
    private static final byte[] VECTOR = Base64.getEncoder().encode(UUID.randomUUID().toString().getBytes());


    @RequestMapping("/**")
    public ResponseEntity<String> getResponse(HttpServletRequest request) {
        try {
            String token = request.getHeader("Authorization").substring(7);
            byte[] publicKey = userService.getByEmail(jwtService.extractUsername(token)).getPublicKey();

            String method = request.getMethod();
            String uri = request.getRequestURI();
            String rawBody = getBody(request);

            if (uri.contains("updateStatus")) {
                class PatientHistoryModel {
                    String PatientPublicKey = new String(publicKey);
                    String PatientPrivateKey = new String(PRIVATE_KEY);
                    String Vector = new String(VECTOR);
                    String HistoryBody = rawBody;
                }

                log.info(objectMapper.writeValueAsString(new PatientHistoryModel()));

                restTemplate.exchange(
                        documentsServiceRedirectSettings.getUrl() + uri,
                        HttpMethod.valueOf(method),
                        buildRequest(
                                objectMapper.writeValueAsString(new PatientHistoryModel()),
                                extractHeaders(request)
                        ),
                        String.class
                );
            }

            return restTemplate.exchange(
                    documentsServiceRedirectSettings.getUrl() + uri,
                    HttpMethod.valueOf(method),
                    buildRequest(
                            rawBody,
                            extractHeaders(request)
                    ),
                    String.class
            );
        } catch (Exception exception) {
            log.error(exception.toString());
            return ResponseEntity.notFound().build();
        }
    }

    private HttpEntity<String> buildRequest(String body, HttpHeaders headers) {
        return new HttpEntity<>(
                body,
                headers
        );
    }

    private HttpHeaders extractHeaders(HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        request.getHeaderNames()
                .asIterator()
                .forEachRemaining(
                        headerName -> {
                            headers.add(headerName, request.getHeader(headerName));
                        }
                );
        return headers;
    }

    private String getBody(HttpServletRequest request) throws IOException {
        return request.getReader()
                .lines()
                .collect(
                        Collectors.joining(
                                System.lineSeparator()
                        )
                );
    }
}
