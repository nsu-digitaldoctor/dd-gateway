package nsu.digital.doctor.gateway.controller;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import nsu.digital.doctor.gateway.model.auth.RegisterRequestDTO;
import nsu.digital.doctor.gateway.model.doctor.DoctorsDTO;
import nsu.digital.doctor.gateway.model.hospital.*;
import nsu.digital.doctor.gateway.security.JwtService;
import nsu.digital.doctor.gateway.service.HospitalService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/hospital")
@RequiredArgsConstructor
public class HospitalController {

    private final HospitalService hospitalService;
    private final JwtService jwtService;

    @PostMapping("/login")
    public ResponseEntity<HospitalAuthResponseDTO> authenticate(
            @RequestBody @Valid HospitalAuthRequestDTO authRequestDTO
    ) {
        return ResponseEntity.ok().body(hospitalService.login(authRequestDTO));
    }

    @PostMapping("/register")
    public ResponseEntity<Long> register(
            @RequestBody @Valid HospitalRegisterDTO registerDTO
    ) {
        return ResponseEntity.ok(hospitalService.register(registerDTO));
    }

    @PostMapping("/registerDoctor")
    public ResponseEntity<HttpStatus> registerDoctor(
            HttpServletRequest request,
            @RequestBody @Valid RegisterRequestDTO registerDTO
    ) {
        String email = jwtService.extractUsername(
                jwtService.extractAccessTokenFromHeader(
                        request.getHeader("Authorization")
                )
        );

        hospitalService.registerDoctor(registerDTO, email);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<HospitalResponseDTO> getAllHospitals() {
        return ResponseEntity.ok(hospitalService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<HospitalDTO> getHospital(
            @PathVariable Long id) {
        return ResponseEntity.ok(hospitalService.getById(id));
    }

    @GetMapping("/{id}/doctors")
    public ResponseEntity<DoctorsDTO> getDoctors(
            @PathVariable Long id) {
        return ResponseEntity.ok(hospitalService.getDoctors(id));
    }
}
