package nsu.digital.doctor.gateway.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import nsu.digital.doctor.gateway.exception.UserAlreadyRegisteredException;
import nsu.digital.doctor.gateway.exception.UserNotFoundException;
import nsu.digital.doctor.gateway.model.auth.AuthRequestDTO;
import nsu.digital.doctor.gateway.model.auth.AuthenticationResponseDTO;
import nsu.digital.doctor.gateway.model.auth.CheckVerificationCodeRequestDTO;
import nsu.digital.doctor.gateway.model.auth.RefreshPasswordDTO;
import nsu.digital.doctor.gateway.model.auth.RefreshTokenRequestDTO;
import nsu.digital.doctor.gateway.model.auth.RegisterRequestDTO;
import nsu.digital.doctor.gateway.model.user.Role;
import nsu.digital.doctor.gateway.model.auth.SendVerificationCodeRequestDTO;
import nsu.digital.doctor.gateway.security.JwtService;
import nsu.digital.doctor.gateway.service.AuthenticationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;



@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
@Validated
public class AuthController {
    private final AuthenticationService authService;
    private final JwtService jwtService;

    @PostMapping("/register")
    public ResponseEntity<HttpStatus> register(
            @RequestBody @Valid RegisterRequestDTO registerDTO,
            @RequestParam (name = "role", required = false) Role role
    ) {
        authService.register(registerDTO, role);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponseDTO> authenticate(
            @RequestBody @Valid AuthRequestDTO authRequestDTO
    ) {
        return ResponseEntity.ok().body(authService.authenticate(authRequestDTO));
    }

    @PostMapping("/token/refresh")
    public ResponseEntity<AuthenticationResponseDTO> refreshToken(@RequestBody @Valid RefreshTokenRequestDTO refreshRequest) {
        AuthenticationResponseDTO response = jwtService.refreshToken(refreshRequest.getRefreshToken());
        if (response == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        return ResponseEntity.ok(response);
    }

    @PostMapping("/send-verify-code")
    public ResponseEntity<?> sendVerificationCode(@RequestBody @Valid SendVerificationCodeRequestDTO requestDTO) {
        authService.sendVerificationCode(requestDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/check-verify-code")
    public ResponseEntity<Boolean> checkCode(@RequestBody @Valid CheckVerificationCodeRequestDTO verificationCodeRequestDTO) {
        return ResponseEntity.ok().body(authService.isCodeValid(verificationCodeRequestDTO));
    }

    @PostMapping("/refresh-password")
    public ResponseEntity<AuthenticationResponseDTO> refreshPassword(@RequestBody @Valid RefreshPasswordDTO refreshPasswordDTO) {
        return ResponseEntity.ok().body(authService.refreshPassword(refreshPasswordDTO));
    }


    @ExceptionHandler({UserNotFoundException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<String> handleException(UserNotFoundException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }

    @ExceptionHandler({UserAlreadyRegisteredException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<String> handleException(UserAlreadyRegisteredException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }
}
