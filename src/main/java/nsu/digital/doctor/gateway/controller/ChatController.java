package nsu.digital.doctor.gateway.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import nsu.digital.doctor.gateway.config.ChatRedirectSettings;
import nsu.digital.doctor.gateway.model.ChatBodyDTO;
import nsu.digital.doctor.gateway.model.user.UserDTO;
import nsu.digital.doctor.gateway.security.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/chat/v1")
@Slf4j
public class ChatController {

    private final JwtService jwtService;

    private final ObjectMapper mapper;

    private final RestTemplate restTemplate;

    private final ChatRedirectSettings chatRedirectSettings;

    @Autowired
    public ChatController(
            JwtService jwtService,
            ObjectMapper mapper,
            RestTemplate restTemplate, ChatRedirectSettings chatRedirectSettings) {
        this.jwtService = jwtService;
        this.mapper = mapper;
        this.restTemplate = restTemplate;
        this.chatRedirectSettings = chatRedirectSettings;
    }

    @RequestMapping("/*")
    public ResponseEntity<String> getResponse(HttpServletRequest request) {
        try {
            String method = request.getMethod();
            String uri = request.getRequestURI();
            String token = request.getHeader("Authorization").strip().split(" ")[1];
            Long tokenUserId = jwtService.extractId(token);
            String rawBody = getBody(request);
            Long bodyUserId = getUserId(rawBody);
            validate(bodyUserId, tokenUserId);
            return restTemplate.exchange(
                    chatRedirectSettings.getUrl() + uri,
                    HttpMethod.valueOf(method),
                    buildRequest(
                            rawBody,
                            extractHeaders(request)
                    ),
                    String.class
            );
        } catch (Exception exception) {
            log.error(exception.toString());
            return ResponseEntity.notFound().build();
        }
    }

    private HttpEntity<String> buildRequest(String body, HttpHeaders headers) {
        return new HttpEntity<String>(
                body,
                headers
        );
    }

    private HttpHeaders extractHeaders(HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        request.getHeaderNames()
                .asIterator()
                .forEachRemaining(
                        headerName -> {
                            headers.add(headerName, request.getHeader(headerName));
                        }
                );
        return headers;
    }

    private void validate(Long bodyUserId, Long tokenUserId) {
        if (!tokenUserId.equals(bodyUserId)) {
            throw new IllegalArgumentException("userId из тела и токена не совпадают");
        }
    }

    private Long getUserId(String rawBody) throws IOException {
        ChatBodyDTO chatBody = mapper.readValue(rawBody, ChatBodyDTO.class);
        return Optional.of(chatBody)
                .map(ChatBodyDTO::getUser)
                .map(UserDTO::getId)
                .orElseThrow(() -> new IllegalStateException("Не найден userId"));
    }

    private String getBody(HttpServletRequest request) throws IOException {
        return request.getReader()
                .lines()
                .collect(
                        Collectors.joining(
                                System.lineSeparator()
                        )
                );
    }

}
