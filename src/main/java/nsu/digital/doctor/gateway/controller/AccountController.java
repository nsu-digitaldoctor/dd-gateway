package nsu.digital.doctor.gateway.controller;

import lombok.RequiredArgsConstructor;
import nsu.digital.doctor.gateway.model.user.AccountDTO;
import nsu.digital.doctor.gateway.service.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/account")
@RequiredArgsConstructor
public class AccountController {

    private final AuthenticationService authService;

    @GetMapping
    public ResponseEntity<AccountDTO> getAccount() {

        return ResponseEntity.ok().body(authService.getAccount());
    }
}
