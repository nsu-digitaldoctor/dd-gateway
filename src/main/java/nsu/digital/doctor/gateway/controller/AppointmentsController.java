package nsu.digital.doctor.gateway.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nsu.digital.doctor.gateway.config.AppointmentsRedirectSettings;
import nsu.digital.doctor.gateway.config.ChatRedirectSettings;
import nsu.digital.doctor.gateway.config.DocumentsServiceRedirectSettings;
import nsu.digital.doctor.gateway.model.ChatBodyDTO;
import nsu.digital.doctor.gateway.model.user.UserDTO;
import nsu.digital.doctor.gateway.security.JwtService;
import nsu.digital.doctor.gateway.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/appointments")
@Slf4j
@RequiredArgsConstructor
public class AppointmentsController {

    private final RestTemplate restTemplate;

    private final AppointmentsRedirectSettings appointmentsRedirectSettings;
    private final DocumentsServiceRedirectSettings documentsServiceRedirectSettings;
    private final UserService userService;
    private final JwtService jwtService;

    private static final String PRIVATE_KEY = new String(Base64.getEncoder().encode(asBytes(UUID.randomUUID())));
    private static final String VECTOR = new String(Base64.getEncoder().encode(asBytes(UUID.randomUUID())));


    @RequestMapping("/**")
    public ResponseEntity<String> getResponse(HttpServletRequest request) {
        try {
            String token = request.getHeader("Authorization").substring(7);
            byte[] publicKey = userService.getByEmail(jwtService.extractUsername(token)).getPublicKey();

            String method = request.getMethod();
            String uri = request.getRequestURI();
            String rawBody = getBody(request);

            if (uri.contains("insertConclusion")) {
                class PatientHistoryModel {
                    String PatientPublicKey = new String(publicKey, 0, 16);
                    String PatientPrivateKey = PRIVATE_KEY;
                    String Vector = VECTOR;
                    String HistoryBody = rawBody;

                    @Override
                    public String toString() {
                        return "{" +
                                "\"PatientPublicKey\":\"" + PatientPublicKey + "\"," +
                                "\"PatientPrivateKey\":\"" + PatientPrivateKey + "\"," +
                                "\"Vector\":\"" + Vector + "\"," +
                                "\"HistoryBody\":\"" + UUID.randomUUID() + UUID.randomUUID() + "\"" +
                                "}";
                    }
                }

                log.info(new PatientHistoryModel().toString());

                restTemplate.exchange(
                        documentsServiceRedirectSettings.getUrl() + "/api/cards/add-history",
                        HttpMethod.valueOf(method),
                        buildRequest(
                                new PatientHistoryModel().toString(),
                                extractHeaders(request)
                        ),
                        String.class
                );
            }
            return restTemplate.exchange(
                    appointmentsRedirectSettings.getUrl() + uri,
                    HttpMethod.valueOf(method),
                    buildRequest(
                            rawBody,
                            extractHeaders(request)
                    ),
                    String.class
            );
        } catch (Exception exception) {
            log.error(exception.toString());
            return ResponseEntity.notFound().build();
        }
    }

    private HttpEntity<String> buildRequest(String body, HttpHeaders headers) {
        return new HttpEntity<>(
                body,
                headers
        );
    }

    private HttpHeaders extractHeaders(HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        request.getHeaderNames()
                .asIterator()
                .forEachRemaining(
                        headerName -> {
                            headers.add(headerName, request.getHeader(headerName));
                        }
                );
        return headers;
    }

    private String getBody(HttpServletRequest request) throws IOException {
        return request.getReader()
                .lines()
                .collect(
                        Collectors.joining(
                                System.lineSeparator()
                        )
                );
    }

    public static byte[] asBytes(UUID uuid) {
        ByteBuffer bb = ByteBuffer.allocate(16);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        return bb.array();
    }
}
