package nsu.digital.doctor.gateway.service;

import lombok.RequiredArgsConstructor;
import nsu.digital.doctor.gateway.model.doctor.DoctorDTO;
import nsu.digital.doctor.gateway.repository.DoctorRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DoctorService {

    private final DoctorRepository doctorRepository;

    public List<DoctorDTO> getAllDoctors() {
        return doctorRepository.findAll().stream().map(doctor -> DoctorDTO.builder()
                .firstName(doctor.getDoctor().getFirstname())
                .middleName(doctor.getDoctor().getMiddleName())
                .lastName(doctor.getDoctor().getLastname())
                .price(doctor.getPrice())
                .hospital(doctor.getHospital().getName())
                .hospitalName(doctor.getHospital().getName())
                .hospitalAddress(doctor.getHospital().getAddress())
                .specialty(doctor.getSpecialty())
                .id(doctor.getId())
                .build()).toList();
    }
}
