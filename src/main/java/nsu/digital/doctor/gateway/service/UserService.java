package nsu.digital.doctor.gateway.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nsu.digital.doctor.gateway.entity.User;
import nsu.digital.doctor.gateway.mapper.UserMapper;
import nsu.digital.doctor.gateway.model.user.UserDTO;
import nsu.digital.doctor.gateway.repository.UserRepository;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserDTO getByEmail(String email) {
        User user = userRepository.findByEmail(email).orElse(null);
        if (user == null) {
            log.warn("No user with email {} found", email);
            return new UserDTO();
        }

        return userMapper.mapToDTO(user);
    }

}