package nsu.digital.doctor.gateway.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nsu.digital.doctor.gateway.config.AppointmentsRedirectSettings;
import nsu.digital.doctor.gateway.config.DocumentsServiceRedirectSettings;
import nsu.digital.doctor.gateway.entity.RefreshPassCode;
import nsu.digital.doctor.gateway.entity.User;
import nsu.digital.doctor.gateway.exception.UserAlreadyRegisteredException;
import nsu.digital.doctor.gateway.exception.UserNotFoundException;
import nsu.digital.doctor.gateway.mapper.UserMapper;
import nsu.digital.doctor.gateway.model.auth.*;
import nsu.digital.doctor.gateway.model.user.AccountDTO;
import nsu.digital.doctor.gateway.model.user.Role;
import nsu.digital.doctor.gateway.repository.RefreshPassCodeRepository;
import nsu.digital.doctor.gateway.repository.UserRepository;
import nsu.digital.doctor.gateway.security.JwtService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authManager;
    private final RefreshPassCodeRepository refreshPassCodeRepository;
    private final MailSenderService mailSenderService;
    private final UserMapper userMapper;

    private final RestTemplate restTemplate;

    private final DocumentsServiceRedirectSettings documentsServiceRedirectSettings;

    private final Integer MAX_SIX_DIGIT_NUMBER = 999999;
    private final String VERIFICATION_EMAIL_SUBJECT = "Verification code from digital doctor";

    public User register(RegisterRequestDTO registerDTO, Role role) {
        String email = registerDTO.getEmail();
        if (!userRepository.findByEmail(registerDTO.getEmail()).isEmpty()) {
            log.warn("This user already registered");
            throw new UserAlreadyRegisteredException("User with such email already registered: " + email);
        }

        byte[] key = Base64.getEncoder().encode(UUID.randomUUID().toString().getBytes());

        ResponseEntity<String> response = restTemplate.exchange(
                documentsServiceRedirectSettings.getUrl() + "/api/cards/add-card/" + new String(key, 0, 16),
                HttpMethod.POST,
                null,
                String.class
        );

        log.info(response.getBody());

        User user = User.builder()
                .firstname(registerDTO.getFirstName())
                .lastname(registerDTO.getLastName())
                .middleName(registerDTO.getMiddleName())
                .email(registerDTO.getEmail())
                .password(passwordEncoder.encode(registerDTO.getPassword()))
                .inn(registerDTO.getInn())
                .publicKey(key)
                .role(role != null ? role : Role.USER)
                .build();

        return userRepository.save(user);
    }

    public AuthenticationResponseDTO authenticate(AuthRequestDTO authDTO) {
        authManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authDTO.getEmail(),
                        authDTO.getPassword()
                )
        );

        User user = userRepository.findByEmail(authDTO.getEmail())
                .orElseThrow(() -> new UserNotFoundException(String.format("User was not found with such email: %s", authDTO.getEmail())));

        String accessToken = jwtService.generateAccessToken(user);
        String refreshToken = jwtService.generateRefreshToken(user);
        return AuthenticationResponseDTO.builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .userId(user.getId())
                .build();
    }

    public AuthenticationResponseDTO refreshPassword(RefreshPasswordDTO refreshPasswordDTO) {
        String email = refreshPasswordDTO.getEmail();
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException(String.format("User was not found with such email: %s", email)));

        user.setPassword(passwordEncoder.encode(refreshPasswordDTO.getPassword()));

        String accessToken = jwtService.generateAccessToken(user);
        String refreshToken = jwtService.generateRefreshToken(user);
        return AuthenticationResponseDTO.builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .build();
    }

    public AccountDTO getAccount() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (Objects.isNull(user)) {
            throw new UserNotFoundException("User not authorized");
        }

        return userMapper.mapToAccountDTO(user);

    }

    public Boolean isCodeValid(CheckVerificationCodeRequestDTO verificationCodeRequestDTO) {
        String email = verificationCodeRequestDTO.getEmail();
        List<RefreshPassCode> refreshPassCodes = refreshPassCodeRepository.findAllByEmailAndExpiryDateAfter(email, Instant.now());

        String code = verificationCodeRequestDTO.getCode();
        for (RefreshPassCode refreshPassCode : refreshPassCodes) {
            if (code.equals(refreshPassCode.getCode())) {
                invalidateOtherCodes(email, refreshPassCode);
                return true;
            }
        }
        return false;
    }

    /**
     * Должен найти user'а в базе и отправить код на его email
     */
    public void sendVerificationCode(SendVerificationCodeRequestDTO requestDTO) {
        String email = requestDTO.getEmail();
        userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException(String.format("User was not found with such email: %s", email)));

        String code = createCode(email);
        mailSenderService.sendEmail(email, VERIFICATION_EMAIL_SUBJECT, code);
    }

    private String createCode(String email) {
        Random random = new Random();
        String code = String.format("%06d", random.nextInt(MAX_SIX_DIGIT_NUMBER));

        refreshPassCodeRepository.save(new RefreshPassCode(email, code, Instant.now().plus(5, ChronoUnit.MINUTES)));

        return code;
    }

    private void invalidateOtherCodes(String email, RefreshPassCode validCode) {
        List<RefreshPassCode> refreshPassCodes = refreshPassCodeRepository.findAllByEmail(email);
        for (RefreshPassCode refreshPassCode : refreshPassCodes) {
            if (!refreshPassCode.equals(validCode)) {
                refreshPassCode.setExpiryDate(Instant.now());
                refreshPassCodeRepository.save(refreshPassCode);
            }
        }
    }

}