package nsu.digital.doctor.gateway.service;

import lombok.RequiredArgsConstructor;
import nsu.digital.doctor.gateway.entity.Doctor;
import nsu.digital.doctor.gateway.entity.Hospital;
import nsu.digital.doctor.gateway.entity.User;
import nsu.digital.doctor.gateway.exception.UserNotFoundException;
import nsu.digital.doctor.gateway.mapper.HospitalMapper;
import nsu.digital.doctor.gateway.model.doctor.DoctorDTO;
import nsu.digital.doctor.gateway.model.doctor.DoctorsDTO;
import nsu.digital.doctor.gateway.model.hospital.*;
import nsu.digital.doctor.gateway.model.auth.RegisterRequestDTO;
import nsu.digital.doctor.gateway.model.user.Role;
import nsu.digital.doctor.gateway.repository.DoctorRepository;
import nsu.digital.doctor.gateway.repository.HospitalRepository;
import nsu.digital.doctor.gateway.repository.UserRepository;
import nsu.digital.doctor.gateway.security.JwtService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class HospitalService {
    private final HospitalRepository hospitalRepository;
    private final UserRepository userRepository;
    private final HospitalMapper hospitalMapper;
    private final AuthenticationService authService;
    private final AuthenticationManager authManager;
    private final JwtService jwtService;
    private final DoctorRepository doctorRepository;

    public HospitalAuthResponseDTO login(HospitalAuthRequestDTO dto) {
        User hospital = userRepository.findByInn(dto.getInn())
                .orElseThrow(() -> new UserNotFoundException(String.format("Hospital was not found with such inn: %s", dto.getInn())));

        authManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        hospital.getEmail(),
                        dto.getPassword()
                )
        );
        Hospital h = hospitalRepository.findByOrganization(hospital);

        String accessToken = jwtService.generateAccessToken(hospital);
        String refreshToken = jwtService.generateRefreshToken(hospital);
        return HospitalAuthResponseDTO
                .builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .id(h.getId())
                .build();
    }

    public Long register(HospitalRegisterDTO dto) {
        RegisterRequestDTO requestDTO =
                RegisterRequestDTO
                        .builder()
                        .firstName(dto.getName())
                        .email(dto.getEmail())
                        .inn(dto.getInn())
                        .password(dto.getPassword())
                        .build();

        User user = authService.register(requestDTO, Role.ORG);

        Hospital hospital = Hospital.builder()
                .name(dto.getName())
                .organization(user)
                .build();

        return hospitalRepository.save(hospital).getId();
    }

    public HospitalResponseDTO getAll() {
        List<Hospital> hospitals = hospitalRepository.findAll();
        HospitalResponseDTO response = new HospitalResponseDTO();
        response.setHospitals(hospitals.stream().map(hospitalMapper::mapToDTO).toList());
        return response;
    }

    public HospitalDTO getById(Long id) {
        return hospitalMapper.mapToDTO(hospitalRepository.getById(id));
    }

    public DoctorsDTO getDoctors(Long id) {
        Hospital hospital = hospitalRepository.getById(id);

        List<Doctor> doctors = hospital.getDoctors();

        return DoctorsDTO
                .builder()
                .doctors(doctors
                        .stream()
                        .map(d -> DoctorDTO
                                .builder()
                                .hospital(hospital.getName())
                                .firstName(d.getDoctor().getFirstname())
                                .lastName(d.getDoctor().getLastname())
                                .middleName(d.getDoctor().getMiddleName())
                                .price(d.getPrice())
                                .specialty(d.getSpecialty())
                                .id(d.getId())
                                .build()
                        )
                        .collect(Collectors.toList())
                )
                .build();
    }

    public void registerDoctor(RegisterRequestDTO dto, String hospitalEmail) {
        User doctor = authService.register(dto, Role.DOCTOR);

        User hospital = userRepository.findByEmail(hospitalEmail)
                .orElseThrow(() -> new UserNotFoundException(String.format("Hospital was not found with such email: %s", hospitalEmail)));

        Doctor doctorEntity = Doctor.builder().doctor(doctor).hospital(hospital.getHospital()).price(dto.getPrice()).specialty(dto.getSpecialty()).build();

        doctorRepository.save(doctorEntity);
    }
}
