package nsu.digital.doctor.gateway.mapper;

import lombok.RequiredArgsConstructor;
import nsu.digital.doctor.gateway.entity.Hospital;
import nsu.digital.doctor.gateway.model.hospital.HospitalDTO;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class HospitalMapper {

    private final UserMapper userMapper;

    public HospitalDTO mapToDTO(Hospital hospital) {
        return HospitalDTO.builder()
                .id(hospital.getId())
                .name(hospital.getName())
                .fullName(hospital.getFullName())
                .address(hospital.getAddress())
                .description(hospital.getDescription())
                //.headDoctor(userMapper.mapToDTO(hospital.getHeadDoctor()))
                .build();
    }
}
