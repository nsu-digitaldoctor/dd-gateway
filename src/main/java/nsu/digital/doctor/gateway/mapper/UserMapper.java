package nsu.digital.doctor.gateway.mapper;

import nsu.digital.doctor.gateway.entity.User;
import nsu.digital.doctor.gateway.model.user.AccountDTO;
import nsu.digital.doctor.gateway.model.user.UserDTO;
import org.springframework.stereotype.Component;


@Component
public class UserMapper {
    public UserDTO mapToDTO(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .email(user.getEmail())
                .firstName(user.getFirstname())
                .lastName(user.getLastname())
                .middleName(user.getMiddleName())
                .inn(user.getInn())
                .publicKey(user.getPublicKey())
                .role(user.getRole())
                .build();
    }

    public AccountDTO mapToAccountDTO(User user) {
        return AccountDTO.builder()
                .id(user.getId())
                .firstName(user.getFirstname())
                .lastName(user.getLastname())
                .middleName(user.getMiddleName())
                .email(user.getEmail())
                .role(user.getRole().name())
                .build();
    }
}

