package nsu.digital.doctor.gateway.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "hospital")
public class Hospital {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String address;
    private Double rating;
    private String description;
    private String fullName;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "head_doctor", referencedColumnName = "id")
    private User headDoctor;

    @OneToMany(mappedBy = "hospital", cascade = CascadeType.ALL)
    private List<Doctor> doctors;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "organization", referencedColumnName = "id")
    private User organization;
}
