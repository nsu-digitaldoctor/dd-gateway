package nsu.digital.doctor.gateway.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

import java.time.Instant;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "refresh_password_code")
public class RefreshPassCode {

    @Id
    @GeneratedValue
    private Long id;

    private String email;

    private String code;

    private Instant expiryDate;

    public RefreshPassCode(String email, String code, Instant expiryDate) {
        this.email = email;
        this.code = code;
        this.expiryDate = expiryDate;
    }
}
