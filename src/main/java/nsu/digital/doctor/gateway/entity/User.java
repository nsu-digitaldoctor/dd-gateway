package nsu.digital.doctor.gateway.entity;

import jakarta.persistence.*;
import lombok.*;
import nsu.digital.doctor.gateway.model.user.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


import java.util.Collection;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_info")
public class User implements UserDetails {
    @Id
    @GeneratedValue
    private long id;

    private String firstname;
    private String lastname;
    private String middleName;
    private String email;
    private String password;
    private String inn;

    private byte[] publicKey;

    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToOne(mappedBy = "organization")
    private Hospital hospital;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}