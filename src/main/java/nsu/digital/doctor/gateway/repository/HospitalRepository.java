package nsu.digital.doctor.gateway.repository;

import nsu.digital.doctor.gateway.entity.Hospital;
import nsu.digital.doctor.gateway.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalRepository extends JpaRepository<Hospital, Long> {
    Hospital findByOrganization(User organization);

}
