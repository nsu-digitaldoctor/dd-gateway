package nsu.digital.doctor.gateway.repository;

import nsu.digital.doctor.gateway.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorRepository extends JpaRepository<Doctor, Long> {
}
