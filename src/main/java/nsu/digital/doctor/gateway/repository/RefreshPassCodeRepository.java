package nsu.digital.doctor.gateway.repository;


import nsu.digital.doctor.gateway.entity.RefreshPassCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
;

import java.time.Instant;
import java.util.List;

@Repository
public interface RefreshPassCodeRepository extends JpaRepository<RefreshPassCode, Long> {
    List<RefreshPassCode> findAllByEmail(String email);
    List<RefreshPassCode> findAllByEmailAndExpiryDateAfter(String email, Instant date);
}
