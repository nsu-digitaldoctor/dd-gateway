package nsu.digital.doctor.gateway.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("documents")
@Getter
@Setter
public class DocumentsServiceRedirectSettings {
    private String url;
}
