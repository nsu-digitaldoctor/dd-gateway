package nsu.digital.doctor.gateway.model.hospital;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HospitalAuthResponseDTO {
    private String accessToken;
    private String refreshToken;
    private Long id;
}
