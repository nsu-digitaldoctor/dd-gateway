package nsu.digital.doctor.gateway.model.auth;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CheckVerificationCodeRequestDTO {

    @Email
    private String email;

    @NotNull
    private String code;
}
