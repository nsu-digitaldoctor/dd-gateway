package nsu.digital.doctor.gateway.model.hospital;

import lombok.Data;

import java.util.List;

@Data
public class HospitalResponseDTO {
    private List<HospitalDTO> hospitals;
}
