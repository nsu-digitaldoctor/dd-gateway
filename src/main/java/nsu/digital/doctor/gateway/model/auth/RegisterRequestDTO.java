package nsu.digital.doctor.gateway.model.auth;

import jakarta.validation.constraints.Email;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegisterRequestDTO {

    private String firstName;

    private String lastName;

    private String middleName;

    @Email
    private String email;

    private String password;

    private String inn;
    private Integer price;
    private String specialty;
}
