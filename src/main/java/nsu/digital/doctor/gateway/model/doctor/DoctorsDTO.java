package nsu.digital.doctor.gateway.model.doctor;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class DoctorsDTO {
    private List<DoctorDTO> doctors;
}
