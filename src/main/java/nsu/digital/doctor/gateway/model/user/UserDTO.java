package nsu.digital.doctor.gateway.model.user;

import lombok.*;
import nsu.digital.doctor.gateway.model.user.Role;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private long id;
    private String firstName;
    private String lastName;
    private String middleName;
    private String email;
    private Role role;
    private String inn;
    private byte[] publicKey;
}