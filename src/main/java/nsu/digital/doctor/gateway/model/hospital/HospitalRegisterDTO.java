package nsu.digital.doctor.gateway.model.hospital;

import lombok.Data;

@Data
public class HospitalRegisterDTO {
    private String name;
    private String inn;
    private String email;
    private String password;
}
