package nsu.digital.doctor.gateway.model.hospital;

import lombok.Data;

@Data
public class HospitalAuthRequestDTO {
    private String inn;
    private String password;
}
