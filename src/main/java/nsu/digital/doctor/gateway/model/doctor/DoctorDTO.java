package nsu.digital.doctor.gateway.model.doctor;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DoctorDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private String middleName;
    private Integer price;
    private String specialty;
    private String hospital;
    private String hospitalAddress;
    private String hospitalName;
}
