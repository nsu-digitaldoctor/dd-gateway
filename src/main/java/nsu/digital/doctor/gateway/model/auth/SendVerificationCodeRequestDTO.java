package nsu.digital.doctor.gateway.model.auth;

import jakarta.validation.constraints.Email;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SendVerificationCodeRequestDTO {

    @Email
    private String email;
}
