package nsu.digital.doctor.gateway.model.user;

public enum Role {
    USER, ADMIN, ORG, DOCTOR
}

