package nsu.digital.doctor.gateway.model.user;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountDTO {

    private long id;

    private String firstName;

    private String lastName;

    private String middleName;

    private String email;

    private String role;
}
