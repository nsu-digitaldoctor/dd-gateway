package nsu.digital.doctor.gateway.model.auth;

import jakarta.validation.constraints.Email;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RefreshPasswordDTO {

    @Email
    private String email;

    private String password;
}
