package nsu.digital.doctor.gateway.model.hospital;

import lombok.Builder;
import lombok.Data;
import nsu.digital.doctor.gateway.model.user.UserDTO;

@Data
@Builder
public class HospitalDTO {
    private Long id;
    private String name;
    private String address;
    private Double rating;
    private String description;
    private String fullName;
    private UserDTO headDoctor;
}
