package nsu.digital.doctor.gateway.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import nsu.digital.doctor.gateway.entity.RefreshToken;
import nsu.digital.doctor.gateway.entity.User;
import nsu.digital.doctor.gateway.mapper.UserMapper;
import nsu.digital.doctor.gateway.model.auth.AuthenticationResponseDTO;
import nsu.digital.doctor.gateway.repository.RefreshTokenRepository;
import nsu.digital.doctor.gateway.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;


import java.math.BigInteger;
import java.security.Key;
import java.security.SecureRandom;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class JwtService {

    @Value("${jwt.secret-key}")
    private String SECRET_KEY;
    @Value("${jwt.access-life-time}")
    private Long ACCESS_LIFE_TIME;

    private final UserDetailsService userDetailsService;
    private final UserRepository userRepository;
    private final RefreshTokenRepository refreshTokenRepository;
    private final UserMapper userMapper;

    public Long extractId(String token) {
        String username = extractUsername(token);
        return userRepository.findByEmail(username)
                .map(User::getId)
                .orElse(null);
    }

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public String getUserEmailFromHeader(String authHeader) {
        String jwt = authHeader.substring(7);
        return extractUsername(jwt);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    public String generateAccessToken(UserDetails userDetails) {
        return generateToken(new HashMap<>(), userDetails, ACCESS_LIFE_TIME);
    }

    public String generateRefreshToken(UserDetails userDetails) {
        String token = new BigInteger(136, new SecureRandom()).toString(32);
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setToken(token);
        refreshToken.setUsername(userDetails.getUsername());
        refreshToken.setExpiryDate(Instant.now().plus(7, ChronoUnit.DAYS)); //7 days

        refreshTokenRepository.save(refreshToken);

        return refreshToken.getToken();
    }
    public AuthenticationResponseDTO refreshToken(String refreshToken) {
        String userEmail = refreshTokenRepository.findByToken(refreshToken).get().getUsername();
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(userEmail);
        if (!validateToken(refreshToken)) {
            return null;
        }

        String newAccessToken = generateAccessToken(userDetails);

        User user = userRepository.findByEmail(userEmail).get();

        return new AuthenticationResponseDTO(newAccessToken, refreshToken, user.getId());
    }

    public void revokeToken(String token) {
        refreshTokenRepository.deleteByToken(token);
    }

    public boolean validateToken(String token) {
        Optional<RefreshToken> optionalToken = refreshTokenRepository.findByToken(token);

        RefreshToken refreshToken = null;
        if (optionalToken.isPresent()) {
            refreshToken = optionalToken.get();
        }

        if (refreshToken == null) {
            return false;
        }

        UserDetails user = userDetailsService.loadUserByUsername(refreshToken.getUsername());

        return optionalToken.get().getExpiryDate().isAfter(Instant.now()) && user != null;
    }

    public String extractAccessTokenFromHeader(String header) {
        return header.substring(7);
    }

    private String generateToken(Map<String, Object> claims, UserDetails userDetails, long duration) {
        return Jwts
                .builder()
                .setClaims(claims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + duration))
                .signWith(getSignInKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    public boolean isAccessTokenValid(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername())) && !isAccessTokenExpired(token);
    }

    private boolean isAccessTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    private Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSignInKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private Key getSignInKey() {
        byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyBytes);
    }
}
